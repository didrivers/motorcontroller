PWM_Fwd = [1600:100:2000];

f1600 = [0.45 0.38 0.38];
f1700 = [1.20 1.22 1.23];
f1800 = [2.62 2.61 2.61];
f1900 = [4.21 4.24 4.25];
f2000 = [5.90 5.89 5.91];

f1600avg = sum(f1600)/3;
f1700avg = sum(f1700)/3;
f1800avg = sum(f1800)/3;
f1900avg = sum(f1900)/3;
f2000avg = sum(f2000)/3;

fpwm_avg = [f1600avg f1700avg f1800avg f1900avg f2000avg]; 

%========================================================

PWM_Bkd = [1400:-100:1000];

b1400 = [0.30 0.30 0.30];
b1300 = [1.08 1.07 1.07];
b1200 = [2.23 2.22 2.22];
b1100 = [3.65 3.68 3.65];
b1000 = [5.21 5.23 5.25];

b1400avg = sum(b1400)/3;
b1300avg = sum(b1300)/3;
b1200avg = sum(b1200)/3;
b1100avg = sum(b1100)/3;
b1000avg = sum(b1000)/3;

bpwm_avg = [-b1400avg -b1300avg -b1200avg -b1100avg -b1000avg]; 

%========================================================

PWM_rgt = [1600:100:2000];
rads_sec_r = [2.3662 0.5748 0.3821 0.2571 0.1998];

%========================================================

PWM_lft = [1400:-100:1000];
rads_sec_l = [1.5127 0.7722 0.3662 0.2515 0.1884]; 

%========================================================

figure
plot(PWM_Fwd, fpwm_avg, 'b-o', 'linewidth', 2);
title('PWM vs Displacement over 3 seconds (forward)');
xlabel('Duty cycle in microseconds');
ylabel('Displacement');
grid on
grid minor

figure
plot(PWM_Bkd, bpwm_avg, 'g-o', 'linewidth', 2);
title('PWM vs Displacement over 3 seconds (backward)');
xlabel('Duty cycle in microseconds');
ylabel('Displacement');
grid on
grid minor

figure
plot(PWM_rgt, rads_sec_r, 'b-o', 'linewidth', 2);
title('Secs/Rad (Clockwise)');
xlabel('Duty cycle in microseconds');
ylabel('Displacement');
grid on
grid minor

figure
plot(PWM_lft, rads_sec_l, 'g-o', 'linewidth', 2);
title('Secs/Rad (Anti-Clockwise)');
xlabel('Duty cycle in microseconds');
ylabel('Displacement');
grid on
grid minor
