#include <ArduinoJson.h>
#include <PID_v1.h>
#include "Servo.h"

StaticJsonBuffer<200> jsonBuffer;
String serialread = "";

Servo throttle;
Servo steering;

//===================================================
//====================PID_Setup======================
//===================================================
double steering_Kp ,steering_Ki, steering_Kd;

int sample_time = 10;       //Period. 10 is 100Hz
//Sampling time should not be faster than the refresh rate of the sensor signal (feedback signal)

int Steering_low = 1000;    //Lower bound of output signal
int Steering_hig = 2000;    //upper bound of output signal

double steer_sensor = 0;    //angle
double steer_out = 0;       //controller output
double steer_reference = 0; //control signal

double throttle_Kp ,throttle_Ki, throttle_Kd;

//Sampling time should not be faster than the refresh rate of the sensor signal (feedback signal)

int throttle_low = 1000;    //Lower bound of output signal
int throttle_hig = 2000;    //upper bound of output signal

double throttle_sensor = 0;    //angle
double throttle_out = 0;       //controller output
double throttle_reference = 0; //control signal

//These are the constructors of the PID algorithm
PID steeringPID(&steer_sensor, &steer_out, &steer_reference, steering_Kp, steering_Ki, steering_Kd, DIRECT);
PID throttlePID(&throttle_sensor, &throttle_out, &throttle_reference, throttle_Kp, throttle_Ki, throttle_Kd, DIRECT);


void setup()
{
  Serial.begin(9600);
  throttle.attach(10);
  steering.attach(9);
  
  steeringPID.SetMode(AUTOMATIC);
  steeringPID.SetSampleTime(10.0);
  steeringPID.SetOutputLimits(Steering_low,Steering_hig);
  
  throttlePID.SetMode(AUTOMATIC);
  throttlePID.SetSampleTime(10.0);
  throttlePID.SetOutputLimits(Steering_low,Steering_hig);
}

void loop()
{

  if (Serial.available() > 0) 
  {
    serialread = Serial.readString();
    JsonObject& root = jsonBuffer.parseObject(serialread);

    throttle_sensor = root["throttle_sensor"];
    throttle_reference = root["throttle_reference"];
    steer_sensor = root["steer_sensor"];
    steer_reference = root["steer_reference"];
    Serial.print(steer_sensor);
    Serial.print(":");
    Serial.print(steer_reference);
    Serial.print("  ");
    Serial.print(throttle_sensor);
    Serial.print(":");
    Serial.println(throttle_reference);
  }

  steeringPID.Compute();
  throttlePID.Compute();
  
  steering.writeMicroseconds(steer_out);
  throttle.writeMicroseconds(throttle_out);
}
