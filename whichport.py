#!/usr/bin/env python

__author__ = "Sivashankar"
__copyright__ = "Copyright 2017, Project MIGO, DiDrivers, ?/u./k"
__credits__ = "Team MIGO"

"""
Description: function returns the COM port at which arduino board is connected
This function can be called globaly as "from whichport import whichport
The returns the COM_Port value as string
"""

import serial
import warnings
import serial.tools.list_ports

def whichport():
    arduino_ports = [
    p.device
    for p in serial.tools.list_ports.comports()
    if 'Arduino' in p.description
    ]
    if not arduino_ports:
        raise IOError("No Arduino found")
    if len(arduino_ports) > 1:
        warnings.warn('Multiple Arduinos found - using the first')

    return arduino_ports[0]
