#!/usr/bin/env python

__author__ = "Sivashankar"
__copyright__ = "Copyright 2017, Project MIGO, DiDrivers, ?/u./k"
__credits__ = "Team MIGO"

"""
Description

"""


from whichport import whichport
import json
import serial, time, sys
import warnings
freq = 10 #sample frequency 
#print(whichport())

def serInit(port):
	try:
		ser = serial.Serial(port, 9600) #whichport()- from whichport.py
		return ser
	except:
		print("IOError: No Arduino Found")
		
def writeJson(CLS, DLS, CAS, DAS, serPort):
"""
#CLS - current linear speed - throttle_sensor(PWM)
#DLS - desired linear speed - throttle_reference(PWM)
#CAS - current angular speed - steer_sensor(PWM)
#DAS - desired angular speed - steer_reference(PWM)
"""
    try:
        time.sleep(freq)
        data = { 'throttle_sensor':CLS,
                 'throttle_reference':DLS,
                 'steer_sensor':CAS,
                 'steer_reference':DAS}
        serPort.write(data)
        #print(json.dumps(data))
    except KeyboardInterrupt:
            print('Done')
            serPort.close()
            sys.exit(0)
			
def iomap(x, in_min, in_max, out_min, out_max):
"""
Transforms the value in input from input range to output range
"""
	return ((x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min)
	
def constrain(x, min_lim, max_lim):
"""
returns input within the given limit
"""
	if x <= min_lim:
		return min_lim
	elif x>= max_lim:
		return max_lim
	else:
		return x	