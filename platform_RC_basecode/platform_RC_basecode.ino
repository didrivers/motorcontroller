#include <PinChangeInt.h>
#include "Servo.h"

//Oguzhan's RC testing code
//This code will read data from an RC reciever and
//print those values to the serial monitor.

//Pin assingment
#define chanel1PIN 8
#define chanel2PIN 9

//BitFlags
#define Chanel1_flag 1
#define Chanel2_flag 2


//=============================================================
//================Variables outside void loop()================
//=============================================================

//glb= Variables accessed inside and outside void loop()
//lcl= variables accessed only inside void loop()
//ext= variables accessed only outsude voide loop()


//Holds flags above
volatile uint8_t gbl_FLAGUPDATE;

//Shared time values. Measured outside loop and copied in.
//Accessed inside and outside void loop()
volatile uint16_t gbl_Chanel1;
volatile uint16_t gbl_Chanel2;

//Used to recored rising edge
uint32_t ext_Chanel1trig;
uint32_t ext_Chanel2trig;



//Servo object delarations

Servo throttle;
Servo steering;


//=============================================================
//=============================================================
//=============================================================

void setup() {
Serial.begin(115200);

PCintPort::attachInterrupt(chanel1PIN, calcCHANNEL1, CHANGE);
PCintPort::attachInterrupt(chanel2PIN, calcCHANNEL2, CHANGE);

throttle.attach(13);
steering.attach(12);

}

void loop() {

static uint8_t lcl_FLAGUPDATE;
static uint16_t lcl_CHANNEL1;
static uint16_t lcl_CHANNEL2;

if(gbl_FLAGUPDATE)
{
  noInterrupts();
  lcl_FLAGUPDATE=gbl_FLAGUPDATE;
  
  if(lcl_FLAGUPDATE & Chanel1_flag)
    {
      lcl_CHANNEL1=gbl_Chanel1;
    }
  if(lcl_FLAGUPDATE & Chanel2_flag)
    {
      lcl_CHANNEL2=gbl_Chanel2;
    }
   gbl_FLAGUPDATE = 0;
   interrupts();

}

//Do something with Duty Cycle here
Serial.print("Channel 1: ");
Serial.print(lcl_CHANNEL1);
Serial.print(" ");
Serial.print("Channel 2: ");
Serial.println(lcl_CHANNEL2);

throttle.writeMicroseconds(lcl_CHANNEL1);
steering.writeMicroseconds(lcl_CHANNEL2);
}

void calcCHANNEL1(){
  if(digitalRead(chanel1PIN) == HIGH)
    {
      ext_Chanel1trig = micros();
    }
  else
    {
      gbl_Chanel1 = (uint16_t)(micros()-ext_Chanel1trig);
      gbl_FLAGUPDATE |= Chanel1_flag;
    }
}

void calcCHANNEL2(){
  if(digitalRead(chanel2PIN) == HIGH)
    {
      ext_Chanel2trig = micros();
    }
  else
    {
      gbl_Chanel2 = (uint16_t)(micros() - ext_Chanel2trig);
      gbl_FLAGUPDATE |= Chanel2_flag;
    }
}


