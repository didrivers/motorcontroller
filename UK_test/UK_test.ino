#include <Servo.h>


//Arduino Pin assignment
#define SAB_S1_PIN 9 //S1 to Arduino pin 9
#define SAB_S2_PIN 10 //S2 to Arduino pin 10

#define SAB_MIN_PWM 1325 // Sabertooth Mininum PWM Range
#define SAB_MAX_PWM 1675 // Sabertooth Mininum PWM Range

#define TURN_ON_DELAY 100
#define TURN_OFF_DELAY 100

//ECU MIN/MAX Values for steering and throttle
/*
 * We are expecting the value from ECU as float, float Ex. -0.12,0.00 
 * THR,STR 
 */
/*
 * THR Forward value is starting from -0.01 to -1.00 //MIN to MAX
 * THR STOP is 0.00
 * THR Backward is starting from 0.01 to 1.00 //MIN to MAX
 */
#define IN_THR_MIN -1.00
#define IN_THR_MAX 1.00

/*
 * STR Left value is starting from -0.01 to -1.00 //MIN to MAX
 * THR Center (or) Straight is 0.00
 * THR Right is starting from 0.01 to 1.00 //MIN to MAX
 */
#define IN_STR_MIN -1.00
#define IN_STR_MAX 1.00

class motor{
	
	public:
	
		//move - allow or disallow moving
		void move(bool true_false){
			//return immediately if no change
			if (true_false == _flags.moving){
				return;
			}
			//set flag indicate whether moving is permitted:
			_flags.moving = true_false;
			
			//turn off the motor if necessary
			if (!_flags.moving ) {
				mstop();
			}
		}
		
		//moving - returns true is motor is currently moving
		bool moving() const { return _flags.moving; }
		
		//update - adjusts motor speed based on ECU input_iterator
		void update(float THR_IN, float STR_IN);
			
		
		void mstop();
		void movefwd(float THR);
		void rotate(float STR);
		// return mapped vale
		float iomap(float x, float in_min, float in_max, float out_min, float out_max)
		{
			return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
		}
		int midPoint() {
			return (((SAB_MAX_PWM - SAB_MIN_PWM) / 2) + SAB_MIN_PWM);
		}
 
		
	private:
		uint32_t min_time = 0;
	
		// flag bitmask
		struct flags_type {
			
			uint8_t moving  : 1;            ///< 1 if we are permitted to move
		} _flags;
		
		
};

//Servo object delarations
//Switch 4 is in the DOWN position, the Sabertooth 2x25 is in Independent mode. In
//Independent mode, the signal fed to S1 directly controls Motor 1 (outputs M1A and M1B) and
//the signal fed to S2 controls Motor 2.

//S1 == motor 1 == MOT_L
//S2 == motor 2 == MOT_R
Servo MOT_L, MOT_R;

void setup() {
  Serial.begin(115200);
  
  MOT_L.attach(SAB_S1_PIN);
  MOT_R.attach(SAB_S2_PIN);

}

void loop() {
   static uint16_t ML, MR;
   /*
     * Input to the Arduino has to be in serial,
     * deponds upun the input data format the code 
     * here has to be finalised
     * .....
     * ....
     * ..
     * .
     * while(Serial1.available() > 0)
     * {
     *    ....
     *    the euThrIn and ecuStrIn
     * }
     */
	
 //motor::update(ecuThrIn, ecuStrIn);   
  

}

void motor::update(float THR_IN, float STR_IN){
	
	if (!moving()){
		move(false);
		return;
	}
	//get the current time
	const uint32_t now = micros();
	bool should_be_moving = _flags.moving;
	// If moving
	if (!_flags.moving) {
		//set the timer if tthis is the first time
		if (min_time == 0) {
			min_time = now;
		}
		else{
			if ((now - min_time) > TURN_ON_DELAY) {
				should_be_moving = true;
				min_time = 0;
			}
		}
	}
	else if (_flags.moving) {
		//set the timer if tthis is the first time
		if (min_time == 0) {
			min_time = now;
		}
		else{
			if ((now - min_time) > TURN_OFF_DELAY) {
				should_be_moving = false;
				min_time = 0;
			}
		}
	}
	// motor need to update
	if (should_be_moving) {
		movefwd(THR_IN);
		rotate(STR_IN);
		_flags.moving = true;
	}
	else {
		motor::mstop();
	}
}

void motor::movefwd(float THR) {
	static uint16_t z;
	THR = constrain(THR, IN_THR_MIN, IN_THR_MAX);
	z = motor::iomap(THR, IN_THR_MIN, IN_THR_MAX, SAB_MIN_PWM, SAB_MAX_PWM);
	z = constrain(z, SAB_MIN_PWM, SAB_MAX_PWM);
	MOT_L.writeMicroseconds(z);
	MOT_R.writeMicroseconds(z);
}

void motor::rotate(float STR) {
	static uint16_t z;
	STR = constrain(STR, IN_STR_MIN, IN_STR_MAX);
	z = motor::iomap(STR, IN_STR_MIN, IN_STR_MAX, SAB_MIN_PWM, SAB_MAX_PWM);
	if (z < motor::midPoint()) {
		
		z = motor::midPoint() + (motor::midPoint() - z);
		z = constrain(z, motor::midPoint(), SAB_MAX_PWM);
		MOT_L.writeMicroseconds(motor::midPoint());
		MOT_R.writeMicroseconds(z);
	}
	else if (z > motor::midPoint()){
		z = motor::midPoint() + (z - motor::midPoint());
		z = constrain(z, motor::midPoint(), SAB_MAX_PWM);
		MOT_L.writeMicroseconds(z);
		MOT_R.writeMicroseconds(motor::midPoint());
	}
	else {
		return 0;
	}
}
void motor::mstop() {
	MOT_L.writeMicroseconds(1500);
	MOT_R.writeMicroseconds(1500);
	_flags.moving = false;
}
	
