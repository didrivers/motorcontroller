#include <ArduinoJson.h>
#include "Servo.h"

//Oguzhan's RC testing code
//This code will read data from an RC reciever and
//print those values to the serial monitor.

//Pin assingment

//Servo object delarations

Servo throttle;
Servo steering;
StaticJsonBuffer<200> jsonBuffer;
String serialread = "";

int S1, S2;
//Testing variables

float  ncuThrIn ;
float  ncuStrIn ;

int incomingByte = 0;

void setup()
{
Serial.begin(115200);

throttle.attach(9);
steering.attach(10);
}

void loop()
{

if (Serial.available() > 0) 
  {

    serialread = Serial.readString();
    JsonObject& root = jsonBuffer.parseObject(serialread);

      ncuThrIn = root["throt"];
      ncuStrIn = root["steer"];
      Serial.print(ncuThrIn);
      Serial.print(":");
      Serial.println(ncuStrIn);

  }

S2 = map(ncuStrIn, 1000, 2000, 1000, 2000);
S1 = map(ncuThrIn, 1000, 2000, 1000, 2000);

throttle.writeMicroseconds(S2);
steering.writeMicroseconds(S1);
Serial.print(S1);
Serial.print(" : ");
Serial.println(S2);
}


