# MIGO Autonomous wheelchair #

## Differential Motor Control ##

### Connections to make: ###
                * Arduino Pin 9  ->  Sabertooth S1 # controls the forward/back motion
                * Arduino Pin 10 ->  Sabertooth S2 # controls the turning motion from Left to Right
                * Arduino GND    ->  Sabertooth 0V
                * Arduino VIN    ->  Sabertooth 5V (OPTIONAL, if you want the Sabertooth to power the Arduino)
### Mode Switch Configuration ###
* Sw_1 --> Up (1) # Analog input mode 
* Sw_2 --> Up (1) # Analog input mode 
* Sw_3 --> Up (1) # NiCd, NiMH or alkaline batteries, or from a power supply
* Sw_4 --> Up (1) # S1 controls the forward/back motion of the vehicle, and the analog signal fed into S2 controls the turning motion of the vehicle. 
* Sw_5 --> Down (1) #  The response to input signals will be exponential. This softens control around the zero speed point, which is useful for control of vehicles with fast top speeds or fast max turning rates.
* Sw_6 --> Up (1) #  The input signal range is from 0V(1000μs) to 5V(2000μs), with a zero point of 2.5V(1500μs). *delay per voltade has to verifyed.


### Control each motor with servo commands ###
* Use 3-argument init: servo.attach(pin, 1000, 2000);
* 0°-90° (1000-1500 μs) reverses
* 90°-180° (1500-2000 μs) goes forward
* 90° (1500 μs) stops

