#include "driver.h"

Servo THR, STR; //Servo objects for steering and throttle
driver d; //driver object
int S1, S2; //Driver SAB_S1_PIN output and SAB_S2_PIN output
void setup()
{
  
  THR.attach(SAB_S1_PIN, SAB_MIN_PWM, SAB_MAX_PWM);
  STR.attach(SAB_S2_PIN, SAB_MIN_PWM, SAB_MAX_PWM);
  
  //Initialize serial and wait for port to open:
  Serial.begin(115200);
  while (!Serial) {
    ; // wait for serial port to connect. Needed for native USB port only
  }

  // sends initialization status with ending line break
  Serial.println("COM_INIT");

  
}

void loop(){

    /*
     * Input to the Arduino has to be in serial,
     * deponds upun the input data format the code 
     * here has to be finalised
     * .....
     * ....
     * ..
     * .
     * while(Serial1.available() > 0)
     * {
     *    ....
     *    the ncuThrIn and ncuStrIn
     * }
     */
    //mapping NCU inputs to driver
    if (ncuThrIn >= IN_THR_MIN && ncuStrIn <= IN_THR_MAX){
    S1 = d.iomap(z, IN_THR_MIN, IN_THR_MAX, SAB_MIN_PWM, SAB_MAX_PWM);
    THR.writeMicroseconds(S1);
    }
    if (ncuStrIn >= IN_STR_MIN && ncuStrIn <= IN_STR_MAX){
    S2 = d.iomap(z, IN_STR_MIN, IN_STR_MAX, SAB_MIN_PWM, SAB_MAX_PWM);
    STR.writeMicroseconds(S2);
    }
;
  
    
  
}

