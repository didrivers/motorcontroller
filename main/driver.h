/*
  control of Sabertooth(SAB) S1, S2 output ranges This can optionally
  be used to provide separation of input and output channel ranges so
  that SAB_MAX_PWM, SAB_MIN_PWM only apply to the input side of Arduino.

  It works by running S1, S2 output calculations as normal, then
  re-mapping the output according to the servo MIN/MAX from
  this object
  
  ### Control each motor with servo commands ###
* Use 3-argument init: servo.attach(pin, 1000, 2000);
* 0°-90° (1000-1500 μs) reverses
* 90°-180° (1500-2000 μs) goes forward
* 90° (1500 μs) stops
###Arduino Connections###
* Arduino Pin 9  ->  Sabertooth S1 # controls the forward/back motion
* Arduino Pin 10 ->  Sabertooth S2 # controls the turning motion from Left to Right

 */
#pragma once

#include <Servo.h>

//Arduino Pin assignment
#define SAB_S1_PIN 9 //S1 to Arduino pin 9
#define SAB_S2_PIN 10 //S2 to Arduino pin 10

#define SAB_MIN_PWM 1000 // Sabertooth Mininum PWM Range
#define SAB_MAX_PWM 2000 // Sabertooth Mininum PWM Range

//ECU MIN/MAX Values for steering and throttle
/*
 * We are expecting the value from ECU as float, float Ex. -0.12,0.00 
 * THR,STR 
 */
/*
 * THR Forward value is starting from -0.01 to -1.00 //MIN to MAX
 * THR STOP is 0.00
 * THR Backward is starting from 0.01 to 1.00 //MIN to MAX
 */
#define IN_THR_MIN -1.00
#define IN_THR_MAX 1.00

/*
 * STR Left value is starting from -0.01 to -1.00 //MIN to MAX
 * THR Center (or) Straight is 0.00
 * THR Right is starting from 0.01 to 1.00 //MIN to MAX
 */
#define IN_STR_MIN -1.00
#define IN_STR_MAX 1.00


class driver
{
 private:
 int num;
 public:
 float iomap(float x, float in_min, float in_max, float out_min, float out_max)
  {
    return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
  }
 void midPoint(){
  return (((SAB_MAX_PWM - SAB_MIN_PWM) / 2) + SAB_MIN_PWM);
  }

}; //driver class end




